import React from 'react'
import { Link } from 'react-router-dom'
// import { Header, Container, Divider, Icon } from "semantic-ui-react";
import Header from 'semantic-ui-react/dist/commonjs/elements/Header'
import Container from 'semantic-ui-react/dist/commonjs/elements/Container'
import Divider from 'semantic-ui-react/dist/commonjs/elements/Divider'
import Icon from 'semantic-ui-react/dist/commonjs/elements/Icon'
import styles1 from '../styles/css/layout.css'
import styles2 from '../styles/scss/layout.scss'

const { pullRight } = styles1
const { h1 } = styles2
// eslint-disable-next-line react/prop-types
const Layout = ({ children }) => (
	// eslint-disable-next-line react/jsx-filename-extension
	<Container>
		<Link to="/">
			<Header as="h1" className={h1}>
        webpack-for-react
			</Header>
		</Link>
		{children}
		<Divider />
		<p className={pullRight}>
      Made with
			<Icon name="heart" color="green" />
      by pc810
		</p>
	</Container>
)

export default Layout
