import React from 'react'
import { hot } from 'react-hot-loader/root'
import importedComponent from 'react-imported-component'
import {
	Switch,
	BrowserRouter as Router,
	Route,
} from 'react-router-dom'
import Layout from './Layout'
import Loading from './Loading'
import Home from './pages/Home'

const AsyncDynamicPAge = importedComponent(
	() => import('./pages/DynamicPage'),
	{
		LoadingComponent: Loading,
	},
)
const AsyncNoMatch = importedComponent(
	() => import('./pages/NoMatch'),
	{
		LoadingComponent: Loading,
	},
)
function App() {
	return (
	// eslint-disable-next-line react/jsx-filename-extension
		<Router>
			<Layout>
				<Switch>
					<Route exact path="/" component={Home} />
					<Route exact path="/dynamic" component={AsyncDynamicPAge} />
					<Route component={AsyncNoMatch} />
				</Switch>
			</Layout>
		</Router>
	)
}

export default hot(App)
