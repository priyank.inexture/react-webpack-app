import React from 'react'
import Icon from 'semantic-ui-react/dist/commonjs/elements/Icon'

function NoMatch() {
	return (
	// eslint-disable-next-line react/jsx-filename-extension
		<>
			<Icon name="minus circle" size="big" />
			<strong>Page not found!</strong>
		</>
	)
}

export default NoMatch
