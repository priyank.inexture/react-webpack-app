import React from 'react'
import { Link } from 'react-router-dom'
import RedSilk from '../../assets/images/red-silk.png'

function Home() {
  return (
    // eslint-disable-next-line react/jsx-filename-extension
    <>
      <p>Hello World of React and Webpack! </p>
      <ul>
        <li>
          <Link to="/dynamic">Navigate to Page</Link>
        </li>
        <li>
          <Link to="/random">Navigate to Invalid Page</Link>
        </li>
      </ul>
      <img src={RedSilk} alt="redsilk" />
    </>
  )
}

export default Home
