import React from 'react'
import Header from 'semantic-ui-react/dist/commonjs/elements/Header'

const DynamicPage = () => (
	// eslint-disable-next-line react/jsx-filename-extension
	<>
		<Header as="h2">Dynamic Page </Header>
		<p>This page was loaded asynchronously!!!</p>
	</>
)

export default DynamicPage
