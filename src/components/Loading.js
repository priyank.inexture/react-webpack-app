import React from 'react'
// import { Loader } from 'semantic-ui-react';
import Loader from 'semantic-ui-react/dist/commonjs/elements/Loader'
import ReactDelayRender from 'react-delay-render'

// eslint-disable-next-line react/jsx-filename-extension
const Loading = () => <Loader active size="massive" />
export default ReactDelayRender({ delay: 300 })(Loading)
