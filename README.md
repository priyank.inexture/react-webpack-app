This is demo application for using webpack with react

# WH Questions

## Why we need webpack?

- It is a tool which helps to manage assets and to write code without worrying the loading & bundling of scripts in efficent way. It also manages the dependency for us.
- It has wide range of plugins which helps to create the structure of our project and manage assets.

## why we need babel ?

- Since we are upgrading our startands of writing Javascript every year. Browsers had to keep up to be able to run.
- Babel comes in as a tool which transpile our higher standard code into backward compatible code.

## why we need eslint ?

- eslint allows to mantain coding standards throughout the project.
- It can easily identify certain classes of bugs, such as those related to variable scope.

## why we needs all of these thing in such a case?

- We will be needing all these when the project size increases it becomes difficult to handle assets, dependancies and manataing coding standards at the same time.
- If we don't have babel we have to constantly required to maintain the order of script loading in our html page. and which scripts is going to use which variable.

- Along with the scope of variable we have to find the way to efficiently load scripts in our pages, so no extra code in included along with the html.

- To help with there webpack comes in rescue.

- Now once we had solved the issue of writing code without worying the dependencies. we have to focus on the possibility that the browser will be able to run our code or not.

- Javascript is evolving fast which makes difficult for browser developers to keep up.

- So we need a transpiler which will convert our js code to backward compatible code. This job is done by babel which also comes with wide range of configurations.

- Maintaing coding stantdards and solving errors at the same time is also very essential part of development.

- This is solved by prettier and eslint.

- prettier helps to save our code in formated manner as per configuration and eslint helps in finding error and automatically fix them upto certain extend.

# About dependencies

## Dependencies

[`prop-types`](https://www.npmjs.com/package/prop-types)
for runtime type checking for React props and similar objects.

[`react`](https://www.npmjs.com/package/react) & [`react-dom`](https://www.npmjs.com/package/react-dom) for react components

[`react-delay-render`](https://www.npmjs.com/package/react-delay-render) used to delay the rendering of components and to avoid flashing of loader if page loads faster than the component itself.

[`react-imported-component`](https://www.npmjs.com/package/react-imported-component) helps with lazy loading of components on routing

[`react-router-dom`](https://www.npmjs.com/package/react-router-dom) for routing

[`semantic-ui-react`](https://www.npmjs.com/package/semantic-ui-react) for styling framework

## devDependencies for webpack, eslint, babel

### **Babel**

[`@babel/core`](https://babeljs.io/docs/en/babel-core/) has core dependencies for Babel

[`babel-loader`](https://webpack.js.org/loaders/babel-loader/) This package allows transpiling JavaScript files using Babel and webpack

[`@babel/preset-env`](https://babeljs.io/docs/en/next/babel-preset-env.html) With this you don’t have to specify if you will be writing ES2015, ES2016 or ES2017. Babel will automatically detect and transpile accordingly.

[`@babel/preset-react`](https://babeljs.io/docs/en/babel-preset-react/) Tells Babel we will be using React

[`@babel/plugin-syntax-dynamic-import`](https://babeljs.io/docs/en/babel-plugin-syntax-dynamic-import) used for dynamic imports

[`@babel/plugin-proposal-class-properties `](https://babeljs.io/docs/en/babel-plugin-proposal-class-properties) to use class properties.

[`react-hot-loader/babel`](https://github.com/gaearon/react-hot-loader) is used in Hot Module Replacement

### **Webpack**

[`webpack`](https://webpack.js.org/) Module bundler

[`webpack-cli`](https://github.com/webpack/webpack-cli) Command Line Interface, needed for Webpack

[`webpack-dev-server`](https://webpack.js.org/configuration/dev-server/) Provides a development server for your application

[`webpack-bundle-analyzer`](https://github.com/webpack-contrib/webpack-bundle-analyzer) Helps to visiualize bundle size and is usefull for optimization

[`webpack-merge`](https://github.com/survivejs/webpack-merge) used to merge comman config with dev or production as per requirement.

[`rimraf`](https://www.npmjs.com/package/rimraf) used to delete dist folder when build command is executed.

### **Eslint**

[`eslint`](https://eslint.org/docs/user-guide/getting-started) use to create [`.eslintrc`](./.eslintrc.json) and linting purpose.

[`eslint-config-prettier`](https://www.npmjs.com/package/eslint-config-prettier) use to lint as per [`.prittierrc`](./.prettierrc) file

[`eslint-config-react`](https://www.npmjs.com/package/eslint-config-react) as we are using react project.

# Flow

## [`Webpack`](https://webpack.js.org/concepts/)

webpack is a tools which manages all the assets throught

[`webapack.config.js`](./webpack.config.js), Used to merge common and dev or prod configuration.

[`webpack.common.js`](./build-utils/webpack.common.js) has all the configuration comman in both producation and developlment

[`webpack.dev.js`](./build-utils/webpack.dev.js) has configuration for developlment

[`webpack.prod.js`](./build-utils/webpack.prod.js) has all the configuration comman in both producation and developlment

### Plugins

- [`HtmlWebpackPlugin`](https://webpack.js.org/plugins/html-webpack-plugin/) used for creation of HTML files to serve your webpack bundles.

- [`HotModuleReplacementPlugin`](https://webpack.js.org/plugins/hot-module-replacement-plugin/) enables Hot Module Replacement, otherwise known as HMR.

- [`MiniCssExtractPlugin`](https://webpack.js.org/plugins/mini-css-extract-plugin/) extracts CSS into separate files. It creates a CSS file per JS file which contains CSS. It supports On-Demand-Loading of CSS and SourceMaps.

- [`BundleAnalyzerPlugin`](https://github.com/webpack-contrib/webpack-bundle-analyzer) use to visiualize bundle size.

### Loaders

- [`style-loader`](https://webpack.js.org/loaders/style-loader) Add exports of a module as style to DOM
- [`css-loader`](https://webpack.js.org/loaders/css-loader) Loads CSS file with resolved imports and returns CSS code
- [`sass-loader`](https://webpack.js.org/loaders/sass-loader) Loads and compiles a SASS/SCSS file
- [`postcss-loader`](https://webpack.js.org/loaders/postcss-loader) Loads and transforms a CSS/SSS file using PostCSS

- [`file-loader`](https://webpack.js.org/loaders/file-loader) Emits the file into the output folder and returns the (relative) URL

## [`Babel`](https://babeljs.io/)

Babel is a toolchain that is mainly used to convert ECMAScript 2015+ code into a backwards compatible version of JavaScript in current and older browsers or environments.

Its configuration are managed through [`.babelrc`](./.babelrc)

### Plugins

- [`@babel/plugin-syntax-dynamic-import`](https://babeljs.io/docs/en/babel-plugin-syntax-dynamic-import) used for dynamic imports
- [`@babel/plugin-proposal-class-properties `](https://babeljs.io/docs/en/babel-plugin-proposal-class-properties) to use class properties.
- [`react-hot-loader/babel`](https://github.com/gaearon/react-hot-loader) is used in Hot Module Replacement

# References

- [`freecodecamp`](https://www.freecodecamp.org/news/learn-webpack-for-react-a36d4cac5060/)
- [`Webpack`](https://webpack.js.org/concepts/)
- [`Babel`](https://babeljs.io/)
